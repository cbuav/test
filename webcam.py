# -*- coding:utf-8-*-

import numpy as np
import cv2

def video_capture(filePath):
    cap_0 = cv2.VideoCapture(0)
    cap_1 = cv2.VideoCapture(1)
    # fps = cap_0.get(cv2.cv.CV_CAP_PROP_FPS)
    fps_0 = cap_0.get(5)
    fps_1 = cap_1.get(5)
    size_0 = (int(cap_0.get(cv2.cv.CV_CAP_PROP_FRAME_WIDTH)),
            int(cap_0.get(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT)))
    size_1 = (int(cap_1.get(cv2.cv.CV_CAP_PROP_FRAME_WIDTH)),
            int(cap_1.get(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT)))
    print fps_0, size_0
    print fps_1, size_1
    fourcc = cv2.cv.FOURCC(*'XVID')
    out = cv2.VideoWriter(filePath, fourcc, 30, size_0)
    if out.isOpened:
        print filePath + " opened!"
    while (cap_0.isOpened()):
        ret_0, frame_0 = cap_0.read()
        ret_1, frame_1 = cap_1.read()
        if ret_0 == True and ret_1 == True:
            # frame_0 = cv2.flip(frame_0, 0)
            # output = cv2.cvtColor(frame_0,  cv2.COLOR_BGR2GRAY)
            cv2.imshow('CAM0', frame_0)
            cv2.imshow('CAM1', frame_1)
            
            out.write(frame_0)

            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
        else:
            break
    cap_0.release()
    cap_1.release()
    out.release()
    cv2.destroyAllWindows()


if __name__ == '__main__':
    filePath = 'output.avi'
    video_capture(filePath)