#!/usr/bin/python
# -*- coding: UTF-8 -*- 
import time

localtime = time.localtime(time.time())
localtime2 = time.asctime( time.localtime(time.time()) )
print "本地时间为 :", localtime
#本地时间为 : time.struct_time(tm_year=2017, tm_mon=6, tm_mday=8, tm_hour=11, tm_min=58, tm_sec=30, tm_wday=3, tm_yday=159, tm_isdst=0)
print "本地时间为 :", localtime2
#本地时间为 : Thu Jun 08 11:58:30 2017

# 格式化成2016-03-20 11:45:39形式
print time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()) 
#2017-06-08 11:58:30

# 格式化成Sat Mar 28 22:24:24 2016形式
print time.strftime("%a %b %d %H:%M:%S %Y", time.localtime()) 
#Thu Jun 08 11:58:30 2017
#1459175064.0
  
# 将格式字符串转换为时间戳
a = "Sat Mar 28 22:24:24 2016"
print time.mktime(time.strptime(a,"%a %b %d %H:%M:%S %Y"))


import calendar
cal = calendar.month(2016, 1)
print "以下输出2016年1月份的日历:"
print cal;

#以下输出2016年1月份的日历:
    January 2016
Mo Tu We Th Fr Sa Su
             1  2  3
 4  5  6  7  8  9 10
11 12 13 14 15 16 17
18 19 20 21 22 23 24
25 26 27 28 29 30 31
